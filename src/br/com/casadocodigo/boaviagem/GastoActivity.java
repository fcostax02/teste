package br.com.casadocodigo.boaviagem;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class GastoActivity extends Activity {
	
	private Spinner categoria;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gasto);
		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.categoria_gasto, android.R.layout.simple_dropdown_item_1line);
	
		this.categoria = (Spinner) findViewById(R.id.categoria);
		this.categoria.setAdapter(adapter);
	
	}
} 
